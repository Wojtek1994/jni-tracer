#include <jni.h>
#include <stdio.h>
#include <string.h>
#include "../include/HelloJNI.h"

JNIEXPORT void JNICALL Java_application_HelloJNI_exceptionMethod
(JNIEnv *env, jobject jo)
{
	jclass exClass;
	char *className = "java/lang/Exception";

	exClass = (*env)->FindClass(env, className);
	(*env)->ThrowNew(env, exClass, NULL);
}


JNIEXPORT jstring JNICALL Java_application_HelloJNI_sayHello___3B
  (JNIEnv *env, jclass cls, jbyteArray bytes)
{
	char buffer [2048];
	int len = (*env)->GetArrayLength(env, bytes);
	jbyte* jniArray = (*env)->GetByteArrayElements(env, bytes, 0);
	memcpy(buffer, jniArray, len);
	return (*env)->NewStringUTF(env, "Return native");
}


JNIEXPORT jstring JNICALL Java_application_HelloJNI_sayHello__I
(JNIEnv *env, jobject jo, jint ji) {
	//printf("jint = %d\n", ji);
	if (ji < 0) return (*env)->NewStringUTF(env, "Argument less than 0!");
	jstring js = (*env)->NewStringUTF(env, "Return native");
	return js;
}


JNIEXPORT jint JNICALL Java_application_HelloJNI_sayHello__
(JNIEnv *env, jobject jo)
{
	//printf("Void JNI!\n");
	return 216;
}

JNIEXPORT jint JNICALL Java_application_HelloJNI_sayHello2__
(JNIEnv *env, jobject jo)
{
	printf("Void JNI 2!\n");
	return 100;
}

JNIEXPORT jint JNICALL Java_application_HelloJNI_testParams
(JNIEnv *env, jobject jo, jobjectArray joa, jstring js, jboolean jb, jbyte jy, jobjectArray joa2,
	jchar jc, jshort jh, jstring js2, jint ji, jlong jl, jfloat jf, jdouble jd, jfloat jf2,
	jdouble jd2, jboolean jb2)
{
	return 128;
}
