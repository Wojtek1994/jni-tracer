/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class application_HelloJNI */

#ifndef _Included_application_HelloJNI
#define _Included_application_HelloJNI
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     application_HelloJNI
 * Method:    sayHello
 * Signature: ([B)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_application_HelloJNI_sayHello___3B
  (JNIEnv *, jclass, jbyteArray);

/*
 * Class:     application_HelloJNI
 * Method:    sayHello
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_application_HelloJNI_sayHello__I
  (JNIEnv *, jobject, jint);

/*
 * Class:     application_HelloJNI
 * Method:    sayHello
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_application_HelloJNI_sayHello__
  (JNIEnv *, jobject);

/*
 * Class:     application_HelloJNI
 * Method:    exceptionMethod
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_application_HelloJNI_exceptionMethod
  (JNIEnv *, jobject);

/*
 * Class:     application_HelloJNI
 * Method:    sayHello2
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_application_HelloJNI_sayHello2
  (JNIEnv *, jobject);

/*
 * Class:     application_HelloJNI
 * Method:    testParams
 * Signature: ([Lapplication/HelloJNI;Ljava/lang/String;ZB[Lapplication/HelloJNI;CSLjava/lang/String;IJFDFDZ)I
 */
JNIEXPORT jint JNICALL Java_application_HelloJNI_testParams
  (JNIEnv *, jobject, jobjectArray, jstring, jboolean, jbyte, jobjectArray, jchar, jshort, jstring, jint, jlong, jfloat, jdouble, jfloat, jdouble, jboolean);

#ifdef __cplusplus
}
#endif
#endif
