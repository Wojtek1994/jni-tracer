$threadsNumber = 10
$callsPerThread = 600
$testsNumber = 10000

$counter = 0
$pinfo = New-Object System.Diagnostics.ProcessStartInfo
$pinfo.FileName = "java"
$pinfo.RedirectStandardError = $false
$pinfo.RedirectStandardOutput = $false
$pinfo.UseShellExecute = $false
$pinfo.Arguments = "-agentpath:D:\JNI\Implementation\x64\Debug\NativeAgent.dll=filters:D:\JNI\Implementation\NativeAgent\config\filters.config,dump:D:\JNI\Implementation\NativeAgent\config\dump.txt -Djava.library.path=D:\JNI\Implementation\x64\Debug\ -jar D:\JNI\Implementation\Tests\JavaApplication\bin\Application.jar $threadsNumber $callsPerThread ""no-exploit"""

for ($i = 1; $i -le $testsNumber; $i += 1)
{
	$p = New-Object System.Diagnostics.Process
	$p.StartInfo = $pinfo
	$p.Start() | Out-Null
	$p.WaitForExit()
	Write-Output "Finished iteration number: $i . Exit code: $($p.ExitCode)"
	if (-Not $p.ExitCode -eq 0)
	{
		$key = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
		$counter += 1
	}
}
Write-Output "Number of errors: $counter"
"Number of errors: $counter
Threads number: $threadsNumber
Calls per thread: $callsPerThread
Tests number: $testsNumber" > result.txt
pause