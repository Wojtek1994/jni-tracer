#!/bin/bash
threadsNumber=7
callsPerThread=600
testsNumber=1

counter=0
iterator=1
exitCode=0
while [ $iterator -le $testsNumber ]; do
	java -agentpath:/home/wwan/Desktop/JNI/Implementation/NativeAgent/bin/libNativeAgent.so=dump:/home/wwan/Desktop/JNI/Implementation/NativeAgent/config/dump.txt,filters:/home/wwan/Desktop/JNI/Implementation/NativeAgent/config/filters.txt -Djava.library.path="/home/wwan/Desktop/JNI/Implementation/Tests/TestNativeLibrary/bin" -jar /home/wwan/Desktop/JNI/Implementation/Tests/JavaApplication/bin/Application.jar $threadsNumber $callsPerThread "no-exploit"
	exitCode=$?
	if [ $exitCode -ne 0 ]; then
		read -n1 -r -p "Program crashed."
		let counter=counter+1
	fi
	echo Iteration number: $iterator. Exit code: $exitCode
	let iterator=iterator+1
done

echo Number of errors: $counter

