package application;



public class HelloJNI implements NativeInterface {

	public static native String sayHello(byte[] bytes);
	public native String sayHello(int i);
	public native int sayHello();
	private native void exceptionMethod();

	public native int sayHello2();
	public native int testParams(HelloJNI[] array, String str, boolean b, byte by, HelloJNI[] array2, char c, short s, String str2, int i, long l, float f, double d, float f2, double d2, boolean b2);
}
