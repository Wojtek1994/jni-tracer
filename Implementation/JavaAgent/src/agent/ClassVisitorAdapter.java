package agent;

import java.util.LinkedList;

import jdk.internal.org.objectweb.asm.ClassVisitor;
import jdk.internal.org.objectweb.asm.MethodVisitor;
import jdk.internal.org.objectweb.asm.Opcodes;

public class ClassVisitorAdapter extends ClassVisitor implements Opcodes {

	
	public static enum FieldDescriptor { BASE_TYPE, OBJECT_TYPE, ARRAY_TYPE, VOID_TYPE };
	
	public static String className;
	
    public ClassVisitorAdapter(final ClassVisitor cv) {
        super(ASM5, cv);
    }
    
    @Override
    public void visit(int arg0, int arg1, String arg2, String arg3,
    		String arg4, String[] arg5) {
    	className = arg2;
    	super.visit(arg0, arg1, arg2, arg3, arg4, arg5);
    }

    @Override
    public MethodVisitor visitMethod(final int access, final String name,
            final String desc, final String signature, final String[] exceptions) {
    	String newName = name;
    	if ((access & Opcodes.ACC_NATIVE) != 0) {
    		newName = "wrapped&" + name;
    		try
    		{
        		generateNativeMethodWrapper(access, desc, signature, exceptions, name, newName);    			
    		}
    		catch (Exception ex)
    		{
    			System.out.println(ex.getMessage() + ", " + ex.getStackTrace());
    		}
    	}
		return cv.visitMethod(access, newName, desc, signature, exceptions);
    }
    
    public static int countArgs(String descriptor) {
    	return getTypesFromDescriptor(getArgumentsFieldsFromDescriptor(descriptor)).size();
    }
    
    public static String getReturnValueType(String descriptor) {
    	return getTypesFromDescriptor(getReturnFieldFromDescriptor(descriptor)).getFirst();
    }
    
    public static int mapTypeToReturnOpcode(String type) {
    	if (type.startsWith("L") || type.startsWith("[")) {
    		return Opcodes.ARETURN;
    	}
    	switch (type) {
    	case "V": return Opcodes.RETURN;
    	case "B": return Opcodes.IRETURN;
    	case "C": return Opcodes.IRETURN;
    	case "D": return Opcodes.DRETURN;
    	case "F": return Opcodes.FRETURN;
    	case "I": return Opcodes.IRETURN;
    	case "J": return Opcodes.LRETURN;
    	case "S": return Opcodes.IRETURN;
    	case "Z": return Opcodes.IRETURN;
    	default: throw new IllegalArgumentException("Unknown parameter type: " + type);
    	}
    }

    public static int mapTypeToLoadOpcode(String type) {
    	if (type.startsWith("L") || type.startsWith("[")) {
    		return Opcodes.ALOAD;
    	}
    	switch (type) {
    	case "V": throw new IllegalArgumentException("Cannot map void type to load opcode.");
    	case "B": return Opcodes.ILOAD;
    	case "C": return Opcodes.ILOAD;
    	case "D": return Opcodes.DLOAD;
    	case "F": return Opcodes.FLOAD;
    	case "I": return Opcodes.ILOAD;
    	case "J": return Opcodes.LLOAD;
    	case "S": return Opcodes.ILOAD;
    	case "Z": return Opcodes.ILOAD;
    	default: throw new IllegalArgumentException("Unknown parameter type: " + type);
    	}
    }
    
    public static String getArgumentsFieldsFromDescriptor(String descriptor) {
    	int startIndex = descriptor.indexOf('(');
    	if (startIndex != 0) {
    		throw new IllegalArgumentException("Method descriptor has invalid format - missing character '(' at position 0.");
    	}
    	int endIndex = descriptor.indexOf(')');
    	if (endIndex == -1) {
    		throw new IllegalArgumentException("Method descriptor has invalid format - missing character ')'.");    		
    	}
    	return descriptor.substring(startIndex + 1, endIndex);
    }
    
    public static String getReturnFieldFromDescriptor(String descriptor) {
    	int startIndex = descriptor.indexOf(')');
    	if (startIndex == -1) {
    		throw new IllegalArgumentException("Method descriptor has invalid format - missing character ')'.");    		
    	}
    	return descriptor.substring(startIndex + 1, descriptor.length());
    }
    
    public static LinkedList<String> getTypesFromDescriptor(String fieldsString) {
    	int i = 0;
    	LinkedList<String> result = new LinkedList<String>();
		boolean isArray = false;
		String fieldString = "";
    	while (i < fieldsString.length()) {
    		switch (getFieldType(fieldsString.charAt(i))) {
    			case VOID_TYPE:	// treat it as base type
	    		case BASE_TYPE: {
	    			if (isArray) {
		    			result.add(fieldString + fieldsString.charAt(i));
		    			fieldString = "";
	    				isArray = false;
	    			} else {
		    			result.add(String.valueOf(fieldsString.charAt(i)));
	    			}
	    			i++;
	    			break;
	    		}
	    		case OBJECT_TYPE: {
	    			int endIndex = fieldsString.indexOf(';', i);
	    			if (endIndex == -1) {
	    				throw new IllegalArgumentException("Method descriptor has invalid format - missing semicolon for object field starting at index " + Integer.toString(i));
	    			}
	    			String objectField = fieldsString.substring(i, endIndex + 1);
	    			if (isArray) {
		    			result.add(fieldString + objectField);
		    			fieldString = "";
	    				isArray = false;
	    			} else {
		    			result.add(objectField);
	    			}
	    			i = endIndex + 1;
	    			break;
	    		}
	    		case ARRAY_TYPE: {
	    			fieldString += fieldsString.charAt(i);
	    			isArray = true;
	    			i++;
	    			break;
	    		}
    		}
    	}
    	return result;
    }
    
    public static FieldDescriptor getFieldType(char field) {
    	switch (field) {
    	case 'V': return FieldDescriptor.VOID_TYPE;
    	case 'B': return FieldDescriptor.BASE_TYPE;
    	case 'C': return FieldDescriptor.BASE_TYPE;
    	case 'D': return FieldDescriptor.BASE_TYPE;
    	case 'F': return FieldDescriptor.BASE_TYPE;
    	case 'I': return FieldDescriptor.BASE_TYPE;
    	case 'J': return FieldDescriptor.BASE_TYPE;
    	case 'S': return FieldDescriptor.BASE_TYPE;
    	case 'Z': return FieldDescriptor.BASE_TYPE;
    	case 'L': return FieldDescriptor.OBJECT_TYPE;
    	case '[': return FieldDescriptor.ARRAY_TYPE;
    	default: throw new IllegalArgumentException("Unkown field descriptor: " + field);
    	}
    }
    
    
    public void generateNativeMethodWrapper(int access, String desc, String signature, String[] exceptions, String name, String newName) {
        MethodVisitor mv = cv.visitMethod(access & (~ACC_NATIVE), name, desc, signature, exceptions);
	    mv.visitCode();

	    mv.visitFieldInsn(Opcodes.GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
	    mv.visitLdcInsn("CALL " + name + " " + desc);
	    mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false);
	    
	    int returnOpcode = mapTypeToReturnOpcode(getReturnValueType(desc));
	    int invokeOpcode = INVOKESTATIC;
	    LinkedList<String> nativeMethodsArgs = getTypesFromDescriptor(getArgumentsFieldsFromDescriptor(desc));
	    int currentStackVariable = 0;
	    
	    if ((access & ACC_STATIC) == 0)
	    {
	    	invokeOpcode = INVOKEVIRTUAL;
	    	mv.visitVarInsn(Opcodes.ALOAD, 0);		// first variable on stack must be 'this'
	    	currentStackVariable++;
	    }
	    
	    // load local variables on stack
	    for (String arg : nativeMethodsArgs)
	    {
	    	int opcode = mapTypeToLoadOpcode(arg);
		    mv.visitVarInsn(opcode, currentStackVariable);
	    	if (opcode == Opcodes.DLOAD || opcode == Opcodes.LLOAD) {
	    		currentStackVariable++;		// doubles and longs take two slots in locals
	    	}
		    currentStackVariable++;
	    }
	    // call native method
	    mv.visitMethodInsn(invokeOpcode, className, newName, desc, false);	    	
	    // call return
        mv.visitInsn(returnOpcode);
        
        mv.visitMaxs(0, 0);
	    mv.visitEnd();
    }
    
}
