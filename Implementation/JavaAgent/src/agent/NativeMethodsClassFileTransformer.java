package agent;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;
import java.util.Arrays;

import jdk.internal.org.objectweb.asm.ClassReader;
import jdk.internal.org.objectweb.asm.ClassVisitor;
import jdk.internal.org.objectweb.asm.ClassWriter;

public class NativeMethodsClassFileTransformer implements ClassFileTransformer {

	public static byte[] transformedClassFileBuffer;
	
	@Override
    public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined,
            ProtectionDomain protectionDomain, byte[] classfileBuffer)
            throws IllegalClassFormatException
    {
		return transform(className, classfileBuffer, classBeingRedefined);
	}


	private byte[] transform(String className, byte[] classfileBuffer, Class<?> classBeingRedefined)
	{
		String transformClass = "HelloJNI";
		if (className.contains(transformClass))
		{
			System.out.println("Transforming " + className);
			ClassReader reader = new ClassReader(classfileBuffer);
			ClassWriter writer = new ClassWriter(reader, ClassWriter.COMPUTE_MAXS | ClassWriter.COMPUTE_FRAMES);
			ClassVisitor classVisitorAdapter = new ClassVisitorAdapter(writer);
			reader.accept(classVisitorAdapter, 0);
			byte[] newBytes = writer.toByteArray();
			transformedClassFileBuffer = Arrays.copyOf(newBytes, newBytes.length);
			return transformedClassFileBuffer;
		}
		else
		{
			return classfileBuffer;
		}
		
	}


}
