package agent;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.Instrumentation;


public class JNIAgent {
	
	public static Instrumentation instrumentation;
	
	public static void premain(String args, Instrumentation inst) {
		System.out.println("Starting agent");
		instrumentation = inst;
		ClassFileTransformer transformer = new NativeMethodsClassFileTransformer();
		instrumentation.addTransformer(transformer, instrumentation.isRetransformClassesSupported());
		inst.setNativeMethodPrefix(transformer, "wrapped&");
	}
	
}
