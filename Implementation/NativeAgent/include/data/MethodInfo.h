#pragma once
#include <string>
#include <list>

typedef struct {
	std::string method_name;
	std::string method_descriptor;
	std::string method_declaring_class;
	std::list<std::string> method_args;
	bool is_static;
	size_t method_address;
} method_info;

