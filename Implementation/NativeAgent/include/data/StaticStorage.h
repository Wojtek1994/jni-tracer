#pragma once
#include <jvmti.h>
#include <map>
#include <mutex>
#include <string>
#include <fstream>
#include "MethodInfo.h"

std::fstream& getFilestream();
void openFileStream(const std::string &filepath, std::ios_base::openmode mode);
void closeFileStream();


std::list<std::pair<std::string, std::string>>& getFilters();
void addFilter(const std::string &method_filter, const std::string &class_filter);


jvmtiEnv* getJvmti();
void setJvmti(jvmtiEnv*);

bool isLivePhase();
void setLivePhase(bool);

bool isFullMode();
void setFullMode(bool);

bool canPrintMethodInfo();
void setPrintMethodInfo(bool);
