#pragma once
#include <map>
#include <mutex>
#include "MethodInfo.h"

void addToBlacklist(size_t);
bool isOnBlacklist(size_t);

bool getMethodByThreadId(method_info * info);
void updateOrInsertMethodByThreadId(method_info&);

bool getMethodByMethodId(const size_t &methodId, method_info *info);
void updateOrInsertMethodByMethodId(size_t, method_info&);
void fulfillMethodInfoByMethodIdMap();
