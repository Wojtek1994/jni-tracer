#pragma once
#include <jni.h>
#include <jvmti.h>

void JNICALL
VMInit(jvmtiEnv *jvmti_env, JNIEnv* jni_env, jthread thread);

void JNICALL
VMDeath(jvmtiEnv *jvmti_env, JNIEnv * jni_env);

void JNICALL
VMStart(jvmtiEnv *jvmti_env, JNIEnv* jni_env);

void JNICALL
NativeMethodBind(jvmtiEnv *jvmti_env,
	JNIEnv* jni_env,
	jthread thread,
	jmethodID method,
	void* address,
	void** new_address_ptr);