#include <jni.h>
#include <jvmti.h>


JNIEXPORT jint JNICALL
Agent_OnLoad(JavaVM *vm, char *options, void *reserved);

JNIEXPORT void JNICALL
Agent_OnUnload(JavaVM *vm);