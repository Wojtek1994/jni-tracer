#pragma once
#include <jvmti.h>
#include "../data/MethodInfo.h"
#include "JavaUtils.h"

const int STATIC_MODIFIER = 8;

jvmtiError getMethodInformation(jvmtiEnv *jvmti_env, jmethodID method, method_info* info);
size_t getMethodIdFromJVMStack();
