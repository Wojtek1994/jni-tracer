#pragma once
#include "../data/MethodInfo.h"

bool isFiltered(const method_info &info);
void printMessage(const std::string &str);
void split(const std::string & s, char delim, std::list<std::string>& elems);
void redirectStandardOutput(const std::string & filepath);
void addFilters(const std::string & filepath);
