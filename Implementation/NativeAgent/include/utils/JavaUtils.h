#pragma once
#include <list>
#include <string>

std::list<std::string> getArgsFromDescriptor(const std::string &descriptor);
size_t getArgsSize(std::list<std::string> args);
