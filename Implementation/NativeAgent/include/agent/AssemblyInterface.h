#pragma once
#if defined(_MSC_VER)
//  Microsoft 
#define EXPORT __declspec(dllexport)
#elif defined(__GNUC__)
//  GCC
#define EXPORT __attribute__((visibility("default")))
#else
#define EXPORT
#pragma warning Unknown dynamic link import/export semantics.
#endif

#include <jni.h>

extern "C" void _redirect(void);		// import from assembly

extern "C" EXPORT size_t getStackSize(size_t methodId);
extern "C" EXPORT size_t logMethodInvocation(JNIEnv * env, ...);