#include "../../include/data/ConcurrentStorage.h"
#include <thread>
#include <algorithm>
#include "../../include/data/StaticStorage.h"
#include "../../include/utils/JvmtiUtils.h"

// list for storing filtered methods. Mutex is needed because of concurrency
std::list<size_t> blacklist;
std::mutex blacklistMutex;

// map for storing currentnly executed method by given thread. Mutex is needed because of concurrency
std::map<std::thread::id, method_info> methodInfoByThreadIdMap;
std::mutex methodByThreadMapMutex;

// map for storing method information by its ID. Mutex is needed because of concurrency
std::map<size_t, method_info> methodInfoByMethodIdMap;
std::mutex methodByIdMapMutex;

// adding a method ID to the blacklist (with locking)
void addToBlacklist(size_t methodId)
{
	while (!blacklistMutex.try_lock()) {} // waiting for lock
	bool blacklistedMethod = std::find(blacklist.begin(), blacklist.end(), methodId) != blacklist.end();
	if (!blacklistedMethod)
	{
		blacklist.push_back(methodId);
	}
	blacklistMutex.unlock();
}

// check if given method ID is on blacklist
bool isOnBlacklist(size_t methodId)
{
	while (!blacklistMutex.try_lock()) {}
	bool blacklistedMethod = std::find(blacklist.begin(), blacklist.end(), methodId) != blacklist.end();
	blacklistMutex.unlock();
	return blacklistedMethod;
}

// adding new or updating an existing information about executed method by current thread
void updateOrInsertMethodByThreadId(method_info &info)
{
	// get ID of the current thread
	std::thread::id currentId = std::this_thread::get_id();
	while (!methodByThreadMapMutex.try_lock()) {} // lock
	std::map<std::thread::id, method_info>::iterator it = methodInfoByThreadIdMap.find(currentId);
	if (it != methodInfoByThreadIdMap.end())
	{
		// delete if given ID already exist
		methodInfoByThreadIdMap.erase(it);
	}
	methodInfoByThreadIdMap.insert(std::pair<std::thread::id, method_info>(currentId, info));
	methodByThreadMapMutex.unlock();
}

// getting method information for currently executing thread.
// method is return via parameter pointer and true/false is returned to indacate if method was found
bool getMethodByThreadId(method_info *info) {
	bool found = false;
	while (!methodByThreadMapMutex.try_lock()) {} // lock
	try
	{
		*info = methodInfoByThreadIdMap.at(std::this_thread::get_id());
		found = true;
	}
	catch (std::out_of_range &ex)	// method does not exist - return false after unlocking
	{
	}
	methodByThreadMapMutex.unlock();
	return found;
}

// getting method information by method ID from methodInfoByMethodIdMap map
// return true/false indating whether the method was found. Method info is returned via parameter pointer
bool getMethodByMethodId(const size_t &methodId, method_info *info) {
	bool found = false;
	while (!methodByIdMapMutex.try_lock()) {} // wait for lock
	try {
		*info = method_info(methodInfoByMethodIdMap.at(methodId));
		found = true;
	}
	catch (std::out_of_range &ex) {
	}
	methodByIdMapMutex.unlock();
	return found;
}

// updating information about method by its ID (in case of changing method ID or re-registering)
void updateOrInsertMethodByMethodId(size_t methodId, method_info &info)
{
	while (!methodByIdMapMutex.try_lock()) {} // wait for lock
	std::map<size_t, method_info>::iterator it = methodInfoByMethodIdMap.find(methodId);
	if (it != methodInfoByMethodIdMap.end())
	{
		methodInfoByMethodIdMap.erase(it);
	}
	methodInfoByMethodIdMap.insert(std::pair<size_t, method_info>(methodId, info));
	methodByIdMapMutex.unlock();
}

// getting method information for all methods in methodInfoByMethodIdMap map by their ID.
void fulfillMethodInfoByMethodIdMap()
{
	while (!methodByIdMapMutex.try_lock()) {} // wait for lock
	for (std::pair<size_t, method_info> pair : methodInfoByMethodIdMap)
	{
		getMethodInformation(getJvmti(), (jmethodID)pair.first, &pair.second);
	}
	methodByIdMapMutex.unlock();
}
