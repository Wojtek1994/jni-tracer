#include "../../include/data/StaticStorage.h"

std::fstream filestream;
std::list<std::pair<std::string, std::string>> filters;
jvmtiEnv* jvmti;
bool livePhase = false;
bool fullMode = false;
bool printMethodInfo = false;

// getter and setter for isLivePhase flag
bool isLivePhase() { return livePhase; }
void setLivePhase(bool value) { livePhase = value; }

// getter and setter for fullMode flag
bool isFullMode() { return fullMode; }
void setFullMode(bool value) { fullMode = value; }

// getter and setter for printMethodInfo flag
bool canPrintMethodInfo() { return printMethodInfo; }
void setPrintMethodInfo(bool value) { printMethodInfo = value; }

// getter and setter for jvmtiEnv pointer
jvmtiEnv* getJvmti() { return jvmti; }
void setJvmti(jvmtiEnv *jvmtiPtr) { jvmti = jvmtiPtr; }

// methods operating on dump file stream
std::fstream& getFilestream() { return filestream; }
void openFileStream(const std::string &filepath, std::ios_base::openmode mode) { filestream.open(filepath.c_str(), mode); }
void closeFileStream() { if (filestream.is_open()) filestream.close(); }

// getter for list of filters
std::list<std::pair<std::string, std::string>>& getFilters() { return filters; }

// method for adding new filter (not concurrently!)
void addFilter(const std::string &method_filter, const std::string &class_filter)
{
	std::pair<std::string, std::string> method_class_pair(method_filter, class_filter);
	filters.push_back(method_class_pair);
}
