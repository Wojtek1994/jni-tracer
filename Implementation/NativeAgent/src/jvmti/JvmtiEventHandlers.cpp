#include "../../include/jvmti/JvmtiEventHandlers.h"
#include <sstream>
#include "../../include/data/MethodInfo.h"
#include "../../include/data/StaticStorage.h"
#include "../../include/data/ConcurrentStorage.h"
#include "../../include/utils/JvmtiUtils.h"
#include "../../include/utils/Utils.h"
#include "../../include/agent/AssemblyInterface.h"

// handler for NativeMethodBind event
// binds new method if it is not filtered and stores essential informtion in methodInfoByMethodIdMap
void JNICALL
NativeMethodBind(jvmtiEnv *jvmti_env,
	JNIEnv* jni_env,
	jthread thread,
	jmethodID method,
	void* address,
	void** new_address_ptr)
{
	method_info info;
	// getting all information
	getMethodInformation(jvmti_env, method, &info);
	info.method_address = (size_t)address;
	size_t* methodIdPtr = (size_t*)method;

	// adding info to map
	updateOrInsertMethodByMethodId(*methodIdPtr, info);

	// evaluate if method should be bound
	bool bindMethod = false;
	if (isFiltered(info))
	{
		addToBlacklist(*methodIdPtr);
	}
	else
	{
		if (isFullMode())
		{
			bindMethod = true;
		}
		else
		{
			if (!isOnBlacklist(*methodIdPtr))
			{
				if (isLivePhase())
				{
					bindMethod = true;
				}
				else
				{
					addToBlacklist(*methodIdPtr);
				}
			}
		}
	}

	// if this should be bound, then log the information about redirecting
	if (bindMethod)
	{
		*new_address_ptr = (void*)_redirect;
		std::stringstream ss;
		ss << "Bind native method. Method name: " << info.method_name << ". From class: " << info.method_declaring_class <<
			". Method descriptor: " << info.method_descriptor << std::endl;
		printMessage(ss.str());
	}
	return;
}

// event fired when changing VM state
// all the method information is now available (start phase)
void JNICALL
VMStart(jvmtiEnv *jvmti_env, JNIEnv* jni_env)
{
	fulfillMethodInfoByMethodIdMap();
	setPrintMethodInfo(true);
}


// event fired when changing VM state
// change the live phase flag to true (live phase)
void JNICALL
VMInit(jvmtiEnv *jvmti_env, JNIEnv* jni_env, jthread thread)
{
	setLivePhase(true);
}

// event fired when changing VM state
// change the live phase flag to false (death phase)
void JNICALL
VMDeath(jvmtiEnv *jvmti_env, JNIEnv* jni_env)
{
	setLivePhase(false);
}