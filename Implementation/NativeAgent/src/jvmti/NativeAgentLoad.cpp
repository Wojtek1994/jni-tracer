#include "../../include/jvmti/NativeAgentLoad.h"
#include "../../include/data/StaticStorage.h"
#include "../../include/utils/Utils.h"
#include "../../include/jvmti/JvmtiEventHandlers.h"
#include "../../include/agent/NativeAgent.h"

// loading agent - reading agent parameters, setting capabilities, events and event handlers
JNIEXPORT jint JNICALL
Agent_OnLoad(JavaVM *vm, char *options, void *reserved) {
	analyseOptions(options);
	jvmtiError error;
	jvmtiCapabilities potentialCapabilities;
	jvmtiCapabilities requiredCapabilities;
	jvmtiEventCallbacks callbacks;
	jvmtiEventNativeMethodBind nativeMethodBindEventHandler = NativeMethodBind;
	jint error_code;
	// setting callbacks (event handlers)
	callbacks.NativeMethodBind = nativeMethodBindEventHandler;
	callbacks.VMInit = VMInit;
	callbacks.VMStart = VMStart;
	callbacks.VMDeath = VMDeath;
	jvmtiEnv* jvmti;
	error_code = vm->GetEnv((void**)&jvmti, JVMTI_VERSION_1_0);
	if (error_code != JNI_OK) {
		printMessage(std::string("Error when getting jvmti environment pointer: ") + std::to_string(error_code) + "\n");
		return 0;
	}
	setJvmti(jvmti);
	// check if JVM supports NativeMethodBindEvent
	error = jvmti->GetPotentialCapabilities(&potentialCapabilities);
	if (error != JVMTI_ERROR_NONE) {
		printMessage(std::string("Error when getting potential capabilities: ") + std::to_string(error) + "\n");
		return 0;
	}
	if (!potentialCapabilities.can_generate_native_method_bind_events) {
		printMessage(std::string("Cannot generate native method bind event.\n"));
		return 0;
	}
	error = jvmti->GetCapabilities(&requiredCapabilities);
	if (error != JVMTI_ERROR_NONE) {
		printMessage(std::string("Error when getting capabilities: ") + std::to_string(error) + "\n");
		return 0;
	}
	// add essential capabiblites (generating native method bind events)
	requiredCapabilities.can_generate_native_method_bind_events = true;
	error = jvmti->AddCapabilities(&requiredCapabilities);
	if (error != JVMTI_ERROR_NONE) {
		printMessage(std::string("Error when adding capabilities: ") + std::to_string(error) + "\n");
		return 0;
	}
	// enable JVMTI_EVENT_NATIVE_METHOD_BIND event
	error = jvmti->SetEventNotificationMode(JVMTI_ENABLE, JVMTI_EVENT_NATIVE_METHOD_BIND, NULL);
	if (error != JVMTI_ERROR_NONE) {
		printMessage(std::string("Error when setting event notification mode: ") + std::to_string(error) + "\n");
		return 0;
	}
	// enable JVMTI_EVENT_VM_START event
	error = jvmti->SetEventNotificationMode(JVMTI_ENABLE, JVMTI_EVENT_VM_START, NULL);
	if (error != JVMTI_ERROR_NONE) {
		printMessage(std::string("Error when setting event notification mode: ") + std::to_string(error) + "\n");
		return 0;
	}
	// enable JVMTI_EVENT_VM_INIT event
	error = jvmti->SetEventNotificationMode(JVMTI_ENABLE, JVMTI_EVENT_VM_INIT, NULL);
	if (error != JVMTI_ERROR_NONE) {
		printMessage(std::string("Error when setting event notification mode: ") + std::to_string(error) + "\n");
		return 0;
	}
	// enable JVMTI_EVENT_VM_DEATH event
	error = jvmti->SetEventNotificationMode(JVMTI_ENABLE, JVMTI_EVENT_VM_DEATH, NULL);
	if (error != JVMTI_ERROR_NONE) {
		printMessage(std::string("Error when setting event notification mode: ") + std::to_string(error) + "\n");
		return 0;
	}
	// set all the callbacks (event handlers)
	error = jvmti->SetEventCallbacks(&callbacks, sizeof(callbacks));
	if (error != JVMTI_ERROR_NONE) {
		printMessage(std::string("Error when setting event callbacks: ") + std::to_string(error) + "\n");
		return 0;
	}
	return 0;
}

// disposing all resources
JNIEXPORT void JNICALL
Agent_OnUnload(JavaVM *vm)
{
	// close file stream
	closeFileStream();
}