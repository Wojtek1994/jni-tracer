
.CODE

extrn getStackSize: PROC
extrn logMethodInvocation: PROC


PUBLIC _redirect
_redirect PROC

	;creating new stack frame
	push RBP
	mov RBP, RSP

	;saving all parameters passed by registers on stack
	;registers RDI, RSI, RCX, RDX, R8 i R9 store 8-bit params and XMM registers store floating point variables
	mov [RBP + 10h], RCX
	mov [RBP + 18h], RDX
	mov [RBP + 20h], R8
	mov [RBP + 28h], R9
	movdqu xmmword ptr [RBP - 10h], XMM0
	movdqu xmmword ptr [RBP - 20h], XMM1
	movdqu xmmword ptr [RBP - 30h], XMM2
	movdqu xmmword ptr [RBP - 40h], XMM3

	;moving stack pointer, so that the stack area for local variables is safe
	sub RSP, 48h

	;allocate home space (32bits)
	sub RSP, 20h
	;pass method ID (RBX) as parameter to getStackSize method
	mov RCX, RBX
	;call method getting size of stack that must be rewritten in new stack frame
	call getStackSize
	;destroy home space and save return value on stack (RBP-048h)
	add RSP, 20h
	mov [RBP - 48h], RAX

rewrite:
	;rewrite RAX bytes of stack from the previous stack frame
	push qword ptr [RBP + RAX]
	sub RAX, 08h
	cmp RAX, 08h
	jz frame_rewritten
	jmp rewrite

frame_rewritten:
	;restore all 8-bit parameters
	mov RCX, [RBP + 10h]
	mov RDX, [RBP + 18h]
	mov R8, [RBP + 20h]
	mov R9, [RBP + 28h]

	;logging method invocation (printing params and method information)
	;this method must return address of the destination method
	call logMethodInvocation

	;restoring all parameters (8-bit and XMM)
	mov RCX, [RBP + 10h]
	mov RDX, [RBP + 18h]
	mov R8, [RBP + 20h]
	mov R9, [RBP + 28h]
	movdqu XMM0, xmmword ptr [RBP - 10h]
	movdqu XMM1, xmmword ptr [RBP - 20h]
	movdqu XMM2, xmmword ptr [RBP - 30h]
	movdqu XMM3, xmmword ptr [RBP - 40h]

	;calling destination method
	call RAX
	
	;restore the stack frame moving back the stack pointer to its initial value
	add RSP, [RBP - 48h]
	add RSP, 40h

	;return to caller
	pop RBP
	ret

_redirect ENDP

END
