
.TEXT

.EXTERN getStackSize: PROC
.EXTERN logMethodInvocation: PROC

.GLOBL _redirect
.TYPE _redirect, @function

_redirect:
	#creating new stack frame
	push %RBP
	mov %RSP, %RBP

	#saving all parameters passed by registers on stack
	#registers RDI, RSI, RCX, RDX, R8 i R9 store 8-bit params and XMM registers store floating point variables
	mov %RDI, -0x8(%RBP)
	mov %RSI, -0x10(%RBP)
	mov %RCX, -0x18(%RBP)
	mov %RDX, -0x20(%RBP)
	mov %R8, -0x28(%RBP)
	mov %R9, -0x30(%RBP)
	movdqu %XMM0, -0x40(%RBP)
	movdqu %XMM1, -0x50(%RBP)
	movdqu %XMM2, -0x60(%RBP)
	movdqu %XMM3, -0x70(%RBP)
	movdqu %XMM4, -0x80(%RBP)
	movdqu %XMM5, -0x90(%RBP)

	#moving stack pointer, so that the stack area for local variables is safe
	sub $0x98, %RSP

	#pass method ID (RBX) as parameter to getStackSize method
	movq %RBX, %RDI
	#call method getting size of stack that must be rewritten in new stack frame
	call getStackSize
	#store return value on stack (at RBP-0x98)
	mov %RAX, -0x98(%RBP)

rewrite:
	#rewrite RAX bytes of stack from the previous stack frame
	pushq (%RBP, %RAX)
	sub $0x8, %RAX
	cmp $0x8, %RAX
	jz frame_rewritten
	jmp rewrite

frame_rewritten:
	#restore all 8-bit parameters
	mov -0x08(%RBP), %RDI
	mov -0x10(%RBP), %RSI
	mov -0x18(%RBP), %RCX
	mov -0x20(%RBP), %RDX
	mov -0x28(%RBP), %R8
	mov -0x30(%RBP), %R9

	#logging method invocation (printing params and method information)
	#this method must return address of the destination method
	call logMethodInvocation

	#restoring all parameters (8-bit and XMM)
	mov -0x08(%RBP), %RDI
	mov -0x10(%RBP), %RSI
	mov -0x18(%RBP), %RCX
	mov -0x20(%RBP), %RDX
	mov -0x28(%RBP), %R8
	mov -0x30(%RBP), %R9

	movdqu -0x40(%RBP), %XMM0
	movdqu -0x50(%RBP), %XMM1
	movdqu -0x60(%RBP), %XMM2
	movdqu -0x70(%RBP), %XMM3
	movdqu -0x80(%RBP), %XMM4
	movdqu -0x90(%RBP), %XMM5

	#calling destination method
	call *%RAX

	#restore the stack frame moving back the stack pointer to its initial value
	add -0x98(%RBP), %RSP
	add $0x90, %RSP

	#return to caller
	pop %RBP
	ret

