#include "../../include/agent/NativeAgent.h"
#include <string>
#include <list>
#include "../../include/data/StaticStorage.h"
#include "../../include/utils/Utils.h"

// analysing given agent parameters
void analyseOptions(char *options)
{
	if (options == NULL)
	{
		return;
	}
	std::string opt(options);
	std::list<std::string> args;
	// splitting options by comma and analysing all of them seperately
	split(opt, ',', args);
	for (std::string arg : args)
	{
		executeArg(arg);
	}
}

void executeArg(const std::string &arg)
{
	// full option (getting method ID from RBX register)
	if (arg.compare("full") == 0)
	{
		setFullMode(true);
		return;
	}
	// additional method/class filters - getting file path and all filters stored there
	std::string fprefix("filters:");
	if (arg.compare(0, fprefix.size(), fprefix) == 0)
	{
		std::string filepath = arg.substr(fprefix.size(), arg.length() - fprefix.size());
		addFilters(filepath);
		return;
	}
	// getting destination dump location (file) - redirecting output
	std::string dprefix("dump:");
	if (arg.compare(0, dprefix.size(), dprefix) == 0)
	{
		std::string filepath = arg.substr(dprefix.size(), arg.length() - dprefix.size());
		redirectStandardOutput(filepath);
	}
}