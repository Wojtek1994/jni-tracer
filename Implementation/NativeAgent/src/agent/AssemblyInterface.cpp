#include "../../include/agent/AssemblyInterface.h"
#include <sstream>
#include <string.h>
#include "../../include/data/MethodInfo.h"
#include "../../include/data/StaticStorage.h"
#include "../../include/data/ConcurrentStorage.h"
#include "../../include/utils/Utils.h"
#include "../../include/utils/JvmtiUtils.h"


// method for calculating how much stack space is used for passing parameters for given method
// the method is given by its ID (stored in methodByMethodIdMap map)
// returns the amount of stack space that should be rewritten
size_t getStackSize(size_t methodId)
{
	method_info info;
	if (!isFullMode())
	{
		// try get the value from JVM stack frame
		methodId = getMethodIdFromJVMStack();
	}
	// get information about method
	bool method_found = getMethodByMethodId(methodId, &info);
	if (!method_found)
	{
		return 0;
	}
	// save information about thread calling the method used in logMethodInvocation
	updateOrInsertMethodByThreadId(info);
	return getArgsSize(info.method_args);
}

// logs method invocation and return the destination method address
// the destination address was saved during a call to getStackSize in methodByThreadIdMap
size_t logMethodInvocation(JNIEnv* env, ...)
{
	std::stringstream logMessage;
	method_info info;
	// method information must be already stored in methodByThreadIdMap map
	if (!getMethodByThreadId(&info))
	{
		return 0;
	}
	if (!canPrintMethodInfo())
	{
		logMessage << "Calling native method. Cannot print any method information in current JVMTI phase." << std::endl;
	}
	else
	{
		logMessage << "Calling method: " << info.method_name.c_str() <<
			", descriptor: " << info.method_descriptor << ", class: " << info.method_declaring_class << std::endl;
		// start reading method parameters
		va_list args;
		va_start(args, env);
		// class or object reference as first argument
		if (info.is_static)
		{
			jclass sender = va_arg(args, jclass);
			logMessage << "The method is static. Class: " << info.method_declaring_class << std::endl;
		}
		else
		{
			jobject sender = va_arg(args, jobject);
			logMessage << "The method is called on object at address: " << (size_t)sender << std::endl;
		}
		// all the arguments printed by their type
		for (std::string str : info.method_args)
		{
			switch (str[0])
			{
			case 'L':
			{
				jobject obj_arg = va_arg(args, jobject);
				if (strcmp(str.c_str(), "Ljava/lang/String;") == 0 && obj_arg != NULL)
				{
					jboolean is_copy;
					const char *jstringVal = env->GetStringUTFChars((jstring)obj_arg, &is_copy);
					logMessage << "Type: " << str << ", Value: " << std::string(jstringVal) << std::endl;
					env->ReleaseStringUTFChars((jstring)obj_arg, jstringVal);
				}
				else
				{
					logMessage << "Type: " << str << ", Address: " << (size_t)obj_arg << std::endl;
				}
				break;
			}
			case '[':
			{
				jobject obj_arg = va_arg(args, jobject);
				logMessage << "Type: " << str << ", Address: " << (size_t)obj_arg << std::endl;
				break;
			}
			case 'Z':
			{
				jint jb = va_arg(args, jint);
				logMessage << "Type: Boolean, Value: " << jb << std::endl;
				break;
			}
			case 'B':
			{
				jint jby = va_arg(args, jint);
				logMessage << "Type: Byte, Value: " << jby << std::endl;
				break;
			}
			case 'C':
			{
				jchar jc = (jchar)va_arg(args, jint);
				logMessage << "Type: Char, Value: " << jc << std::endl;
				break;
			}
			case 'S':
			{
				jshort js = (jshort)va_arg(args, jint);
				logMessage << "Type: Short, Value: " << js << std::endl;
				break;
			}
			case 'I':
			{
				jint ji = va_arg(args, jint);
				logMessage << "Type: Integer, Value: " << ji << std::endl;
				break;
			}
			case 'J':
			{
				jlong jl = va_arg(args, jlong);
				logMessage << "Type: Long, Value: " << jl << std::endl;
				break;
			}
			case 'F':
			{
				float f;
				jdouble jd = va_arg(args, jdouble);
				char *fptr = (char*)&f, *dptr = (char*)&jd;
				memcpy(fptr, dptr, sizeof(f));
				logMessage << "Type: Float, Value: " << f << std::endl;
				break;
			}
			case 'D':
			{
				jdouble jd = va_arg(args, jdouble);
				logMessage << "Type: Double, Value: " << jd << std::endl;
				break;
			}
			}
		}
		va_end(args);
	}
	// printing all the message
	printMessage(logMessage.str());
	// returning destination address
	return info.method_address;
}
