#include "../../include/utils/JvmtiUtils.h"
#include "../../include/data/StaticStorage.h"
#include "../../include/utils/Utils.h"

// getting current method ID from JVM stack
size_t getMethodIdFromJVMStack()
{
	jvmtiError error;
	jvmtiFrameInfo lastFrame;
	jint count;
	// get last stack frame
	error = getJvmti()->GetStackTrace(NULL, 0, 1, &lastFrame, &count);
	if (error != JVMTI_ERROR_NONE) {
		printMessage(std::string("Error getting stack trace information when redirecting to original native method: ") +
			std::to_string((size_t)error) +
			std::string(". This problem occurs when not running full mode and most likely causes JVM crash. ") +
			std::string("It happens when the instrumented application has already finished and JVM calls a method in death phase. ") +
			std::string("You can get rid of this error by running full mode or filtering crash-causing method."));
		return 0;
	}
	// get method ID from frame
	size_t* methodIdPtr = (size_t*)lastFrame.method;
	return *methodIdPtr;
}


// getting all the method information by its ID.
// the information include: method name, signature (descriptor), class name, and method modifiers
jvmtiError getMethodInformation(jvmtiEnv *jvmti_env, jmethodID method, method_info* info)
{
	jclass declaringClass;
	jvmtiError error;
	jint modifiers;
	char *method_name, *method_signature, *method_generic_ptr, *class_signature, *class_generic_ptr;
	error = jvmti_env->GetMethodName(method, &method_name, &method_signature, &method_generic_ptr);
	if (error != JVMTI_ERROR_NONE)
	{
		return error;
	}
	(*info).method_name = std::string(method_name);
	(*info).method_descriptor = std::string(method_signature);
	(*info).method_args = getArgsFromDescriptor((*info).method_descriptor);
	jvmti_env->GetMethodDeclaringClass(method, &declaringClass);
	if (error != JVMTI_ERROR_NONE)
	{
		return error;
	}
	jvmti_env->GetClassSignature(declaringClass, &class_signature, &class_generic_ptr);
	if (error != JVMTI_ERROR_NONE)
	{
		return error;
	}
	(*info).method_declaring_class = std::string(class_signature);
	jvmti_env->GetMethodModifiers(method, &modifiers);
	if (error != JVMTI_ERROR_NONE)
	{
		return error;
	}
	(*info).is_static = modifiers & STATIC_MODIFIER;
	return JVMTI_ERROR_NONE;
}