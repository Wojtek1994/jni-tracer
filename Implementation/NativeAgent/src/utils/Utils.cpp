#include "../../include/utils/Utils.h"
#include <iostream>
#include <sstream>
#include "../../include/data/StaticStorage.h"

// add all filtered methods to filters list. The filtered methods are stored in file that the path to is
// passed as an argument
void addFilters(const std::string &filepath)
{
	std::fstream fs;
	fs.open(filepath.c_str(), std::fstream::in);
	std::string methodName, className;
	if (fs.is_open())
	{
		while (fs >> methodName >> className)
		{
			addFilter(methodName, className);
		}
		fs.close();
	}
}

// redirecting standard output to file given by filepath.
void redirectStandardOutput(const std::string &filepath)
{
	openFileStream(filepath, std::fstream::out);
}

// check if the given method is filtered by its method name and class name
bool isFiltered(const method_info &info) {
	for (std::pair<std::string, std::string> p : getFilters())
	{
		if ((p.first.compare(info.method_name) == 0 || p.first.compare("*") == 0)
			&&
			(p.second.compare(info.method_declaring_class) == 0 || p.second.compare("*") == 0))
		{
			return true;
		}
	}
	return false;
}

// print message (to file or standard output)
void printMessage(const std::string &str)
{
	std::fstream& stream = getFilestream();
	if (stream.is_open())
	{
		stream << str << std::endl;
	}
	else
	{
		std::cout << str << std::endl;
	}
}

// splitting string (s) by delimeter (delim). Returns list of elements
void split(const std::string &s, char delim, std::list<std::string> &elems) {
	std::stringstream ss;
	ss.str(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
}