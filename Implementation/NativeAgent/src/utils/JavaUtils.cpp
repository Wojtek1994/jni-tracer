#include "../../include/utils/JavaUtils.h"

// getting arguments size from list of arguments
// the arguments are shorten to single letters (as in Java descriptors)
size_t getArgsSize(std::list<std::string> args)
{
	// the base size is 16 - JNIEnv pointer (8 bytes) and class/sender object reference (8 bytes)
	int size = 16;
	// each argument has its size - adding all of them
	for (std::string arg : args)
	{
		switch (arg[0])
		{
		case 'D':
			size += 16;
			break;
		case 'L':
		case '[':
		case 'J':
		case 'F':

		case 'Z':
		case 'B':
		case 'C':
		case 'S':
		case 'I':
			size += 8;
			break;
		}
	}
	// on x64 platforms stack must be alligned to 16bytes
	size = size - size % 16 + 16;
	// on Windows there is home space (32bytes). Adding basic size it gives 48bytes.
	// this is minimum stack size to be rewritten
	int minValue = 48;
	return size < minValue ? minValue : size;
}

// getting list of arguments from method descriptor
// example: descriptor [BLjava.lang.String;II gives as result array containing:
// '[B', 'Ljava.lang.String;', 'I', 'I'
std::list<std::string> getArgsFromDescriptor(const std::string &descriptor)
{
	std::list<std::string> args;
	size_t char_iterator = 1;
	std::string arg;
	size_t length = descriptor.length();
	// reading all chars from descriptor
	while (char_iterator < length)
	{
		switch (descriptor.at(char_iterator))
		{
		// if this is array then read the type of variables stored in it later
		case '[':
		{
			arg += descriptor[char_iterator];
			char_iterator++;
			break;
		}
		// object type
		case 'L':
		{
			size_t end_index = descriptor.find(';', char_iterator);
			arg.append(descriptor.substr(char_iterator, end_index - char_iterator + 1));
			args.push_back(arg);
			arg.clear();
			char_iterator = end_index + 1;
			break;
		}
		// end of descriptor
		case ')': return args;
		// any simple type (ints, longs, etc)
		default:
		{
			arg += descriptor[char_iterator];
			args.push_back(arg);
			arg.clear();
			char_iterator++;
			break;
		}
		}
	}
	return args;
}